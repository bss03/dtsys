module Main where

-- base
import Control.Monad ((>=>))
import Control.Monad.Trans.Except (ExceptT, runExceptT)

-- llvm-hs
import LLVM.CodeGenOpt
import LLVM.CodeModel as CodeModel
import LLVM.Context
import LLVM.Module
import LLVM.Relocation
import LLVM.Target

-- llvm-hs-pretty
import LLVM.Pretty

-- text
import qualified Data.Text.Lazy.IO as TIO

-- (this package)
import Lib

main :: IO ()
main = withContext $ \ctx -> do
	putStrLn "Context acquired."
	TIO.putStrLn $ ppllvm mainModule
	withModuleFromAST ctx mainModule $ \modl -> do
		putStrLn "Module created."
		writeLLVMAssemblyToFile (File "true.llvm") modl
		writeBitcodeToFile (File "true.bc") modl
		withHostTargetMachine $ \mach -> do
			putStrLn "Target Machine active."
			hostTriple <- getTargetMachineTriple mach
			hostCpu <- getHostCPUName
			hostFeatures <- getHostCPUFeatures
			initializeNativeTarget
			(tgt, _) <- lookupTarget Nothing hostTriple
			withTargetOptions $ \opts -> do
				withTargetMachine tgt hostTriple hostCpu hostFeatures opts PIC CodeModel.Default Aggressive $ \picMach -> do
					putStrLn "PIC Machine active."
					writeTargetAssemblyToFile picMach (File "true.asm") modl
					writeObjectToFile picMach (File "true.o") modl

printExcept :: ExceptT String IO () -> IO ()
printExcept = runExceptT >=> either putStrLn return
